#!/usr/bin/env bash

# call as startFlow Sp Sm N tol path/to/store/coeffs.csv
# Sp is the ratio between L+ and R
# Sm is the ratio between L- and R
# N is the number of the used series coefficients
# tol is the tolerance of the underlying bisection method 
./bin/startFlow 1e-6 1e-6 10 1e-40 $(pwd)/dat/startupCoeffsCpp.csv

# comparison between python implementation and C++ implementation
# requires arbitrary precisionarithmetics 
#./bin/compCoeffs \
#    $(pwd)/dat/startupCoeffsPython.csv \
#    $(pwd)/dat/startupCoeffsCpp.csv \
#    $(pwd)/dat/startupCoeffsAbsError.csv \
#    $(pwd)/dat/startupCoeffsRelError.csv
