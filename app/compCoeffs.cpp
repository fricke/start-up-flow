/*
 * Author:
 *  Dirk Gruending
 *
 * Description:
 *  Comparison of series coefficients for the Navier slip starting
 *  flow solution. The utility loads the coefficients from files and writes a
 *  file with the relative and absolute differences. 
 *  An extra C++-utility has been created as arbitrary-precision arithmetics
 *  are used for the comparison of the coefficients if a precision beyond the
 * double standard is required.
*/
#include<iostream>
#include<iomanip>
#include<limits>
#include<fstream>
#include<vector>
#include<string>
#include<map>
#include<sstream>

#include <boost/multiprecision/cpp_dec_float.hpp>

template<typename Float>
std::pair<Float,Float> readCoeffLine(std::string& line)
{
    std::stringstream          lineStream(line);
    std::string                cell;

    std::vector<std::string> strCoeffs;

    while(std::getline(lineStream, cell, ','))
    {
        strCoeffs.push_back(cell);
    }
    // This checks for a trailing comma with no data after it.
    if (!lineStream && cell.empty())
    {
        // If there was a trailing comma then add an empty element.
        strCoeffs.push_back("");
    }
    return std::make_pair(Float(strCoeffs[0]),Float(strCoeffs[1]));
}


template<typename Float>
std::vector<std::pair<Float,Float>> readCoeffs(std::string& filePath)
{
    std::vector<std::pair<Float,Float>> coeffs;

    std::string line;
    std::ifstream file;
    file.open(filePath);

    if (file.is_open())
    {
        while( getline(file,line) )
        {
            coeffs.push_back(readCoeffLine<Float>(line));
        }
    }
    file.close();
    return coeffs;
}

int main(int argc, char* args[])
{
    std::cout << "\nStarting compCoeffs." << std::endl;
    using Float = boost::multiprecision::cpp_dec_float_50;

    std::cout
        << std::scientific
        << std::setprecision(std::numeric_limits<double>::digits10);
    std::cout
        << "Numeric limit for double: "
        << std::numeric_limits<double>::digits10 << '\n'
        << "Numeric limit for Float: "
        << std::numeric_limits<Float>::digits10 << std::endl;

    std::map<std::string,std::string> filePaths;
    if(argc != 5)
    {
        std::cout << "No reference file provided. Call as\n"
          << " /path/to/pythonCoeffs.csv"
          << " /path/to/cppCoeffs.csv"
          << " /path/to/abs_erros.csv"
          << " /path/to/rel_erros.csv\n";
        return 0;
    } else
    {
        filePaths.insert(std::make_pair("python",args[1]));
        filePaths.insert(std::make_pair("cpp",args[2]));
        filePaths.insert(std::make_pair("abs_error",args[3]));
        filePaths.insert(std::make_pair("rel_error",args[4]));
    }

    std::cout << "Reading python coeffs:\n" << filePaths["python"] << std::endl;
    auto pythonCoeffs = readCoeffs<Float>(filePaths["python"]);
    std::cout << "Reading cpp coeffs:\n" <<  filePaths["cpp"] << std::endl;
    auto cppCoeffs = readCoeffs<Float>(filePaths["cpp"]);

//    std::cout << "Python coefficients:\n";
//    for(auto val : pythonCoeffs)
//        std::cout << std::setprecision(std::numeric_limits<double>::digits10)
//            << "kn: " << std::get<0>(val)
//            << " An: " << std::get<1>(val) << '\n';
//
//    std::cout << std::endl;
//
//    std::cout << "\nC++ coefficients:\n";
//    for(auto val : cppCoeffs)
//        std::cout << std::setprecision(std::numeric_limits<Float>::digits10)
//            << "kn: " << std::get<0>(val)
//            << " An: " << std::get<1>(val) << '\n';

    std::cout << "\nError comparison:\n";
    std::ofstream absErrorFile;
    std::ofstream relErrorFile;
    absErrorFile.open(filePaths["abs_error"]);
    relErrorFile.open(filePaths["rel_error"]);
    for(auto [itPy,itCpp] = std::tuple(pythonCoeffs.begin(),cppCoeffs.begin());
        itPy != pythonCoeffs.end() && itCpp != cppCoeffs.end(); itPy++, itCpp++)
    {
        std::cout << std::setprecision(std::numeric_limits<Float>::digits10)
            << "kn: " << abs(std::get<0>(*itPy) - std::get<0>(*itCpp))
            << " An: " << abs(std::get<1>(*itPy) - std::get<1>(*itCpp)) << '\n';
        absErrorFile
            << std::setprecision(std::numeric_limits<Float>::digits10)
            << abs(std::get<0>(*itPy) - std::get<0>(*itCpp)) << ','
            << abs(std::get<1>(*itPy) - std::get<1>(*itCpp)) << '\n';
        relErrorFile
            << std::setprecision(std::numeric_limits<Float>::digits10)
            << abs((std::get<0>(*itPy) - std::get<0>(*itCpp))/std::get<0>(*itCpp)) << ','
            << abs((std::get<1>(*itPy) - std::get<1>(*itCpp))/std::get<1>(*itCpp)) << '\n';
    }


    // write normalized deviations to be plotted in python
    std::cout << "\nEnding compCoeffs." << std::endl;
    return 0;
}
