/*
 * Author: Dirk Gruending
 * Description: Computation of series coefficients  for Navier slip start up
 * flow.
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdint>
#include <array>
#include <map>
#include <string>

//#include <boost/math/constants/constants.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

#include "channelFlows.hpp"
#include "startingFlows.hpp"


int main(int argc, char* args[])
{
    std::cout << "\nStarting startFlow." << std::endl;
    std::vector<std::string> para(args, args+argc);
    if(para.size() != 6)
    {
        std::cout << "No target file provided. Call as\n"
          << "\tstartFlow" << " S_+ S_- N tol eps /path/to/cppCoeffs.csv" << std::endl;
        return 0;
    }

    using Float = boost::multiprecision::cpp_dec_float_50;
    std::cout << std::setprecision(std::numeric_limits<Float>::digits10);
    std::cout << "digits10: " 
            << std::numeric_limits<Float>::digits10 << std::endl;
    
    const auto Sp = Float(para[1]);
    const auto Sm = Float(para[2]);
    const auto N = std::size_t(std::stoi(para[3]));
    const auto tol = Float(para[4]);
    const std::string cppFilePath = args[5];
    std::cout << "Writing output to: " << cppFilePath << std::endl;

//    // testing double to Float conversion:
//    std::map<std::string,Float> test = {
//        {"1.0", 1.0}, {"2.0", 2.0}, {"0.1", 0.1}, {"0.01", 0.01}, {"0.001", 0.001}
//    };
//    std::cout << "double to Float conversion:\n";
//    for(auto& [str, val] : test)
//        std::cout << str << " " << val << std::endl;

    std::cout
            << "\nSetup: " << '\n'
            << "Sp: " << Sp << "  "
            << "Sm: " << Sm << "  "
            << "tol: " << tol << "  "
            << "N: " << N << "  "
            << std::endl << std::endl;

    /*
     * writing the first N coefficients
     */
    auto coeffs = ssf::StartupCoeffs<Float>(Sp, Sm, tol);
    const auto& kns = coeffs.kns(N);
    const auto& ans = coeffs.ans(N);
    for(auto k : kns) std::cout << "k: " << k << std::endl;
    std::cout << std::endl;
    for(auto a : ans) std::cout << "a: " << a << std::endl;

    std::size_t n = 0;
    std::ofstream file;
    file.open(cppFilePath, std::fstream::out);
    file << std::setprecision(std::numeric_limits<Float>::digits10);

    for(auto [kn, an] = std::pair(kns.begin(), ans.begin());
        kn != kns.end(), an != ans.end(); ++kn, ++an)
    {
        // write to file
        file << *kn << "," << *an;
        if(kn != kns.end()) file << '\n';

        // provide overview
        std::cout << '\n' << "k_" << n << ": " << *kn << "  "
                  << "a_" << n++ << ": " << *an;
    }
    file.close();

    std::cout << "\nEnding startFlow." << std::endl;
    return 0;
}
