#include <iostream>

#include <boost/multiprecision/cpp_dec_float.hpp>

#include "channelFlows.hpp"
#include "startingFlows.hpp"

int main(int argc, char* args[]) {

    std::cout << "\nStarting startFlow." << std::endl;
    std::vector<std::string> para(args, args+argc);

    using Float = boost::multiprecision::cpp_dec_float_50;
    std::cout << std::setprecision(std::numeric_limits<Float>::digits10);
    std::cout << "digits10: " 
            << std::numeric_limits<Float>::digits10 << std::endl;
    
    const auto Sp = Float(para[1]);
    const auto Sm = Float(para[2]);
    const auto N = std::size_t(std::stoi(para[3]));
    const auto tol = Float(para[4]);

    // testing for this Float
    using Float = boost::multiprecision::cpp_dec_float_50;

    auto coeffs = ssf::StartupCoeffs<Float>(Sp, Sm, tol);
    auto kns = coeffs.kns(N);
    auto ans = coeffs.ans(N);

    int n = 0;
    Float series, sinTerm, cosTerm, expTerm = 0;
    for(auto [itKn, itAn] = std::tuple(kns.begin(), ans.begin());
        itKn != kns.end(),  itAn != ans.end(); ++itKn, ++itAn) {
        std::cout << "k_" << n << *itKn 
                  << " a_" << n++ << " " << *itAn 
                  << std::endl;
    }
    return 0;
}
