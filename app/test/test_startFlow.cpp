#include <iostream>

#include "channelFlows.hpp"
#include "startingFlows.hpp"

int main(int argc, char* args[]) {

    // test setup
    const int Nt = 100;
    const std::string s_dt("0.1");
    const std::string s_x("0.5");
    const std::string s_Sp("0.001");
    const std::string s_Sm("0.001");

    Float dt = Float(s_dt);
    Float x = Float(s_x);
    Float Sp = Float(s_Sp);
    Float Sm = Float(s_Sm);

    // initialize time steps
    std::array<Float,Nt> times;
    auto timeSteper = [&dt, n=0]() mutable {return dt*n++;};
    std::generate_n(times.begin(), times.size(), timeStepper);

    /*
     * analytic solution based on:
     * Matthew and Hasties, 2012
    */
    for(const auto& ti : times)
        std::cout << "\nt: "  << ti
                  << " v: " << cf::navierSlip(ti, x, Sp, Sm, N);
    std::cout << std::endl;
    return 0;
}
