/*
 * Author: Dirk Gruending
 * Description: Computation of series coefficients no slip transient starting
 * flow with arbitrary precision. Faster than the slip version, though the 
 * implementation for the slip case contains the no slip case as special case.
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdint>
#include <string>

#include <boost/math/constants/constants.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

namespace noSlip
{

template<typename Float>
Float kn(const std::size_t n)
{
    using boost::math::constants::pi;
    return n* pi<Float>() /2.0;
}

template<typename Float>
Float An(const std::size_t n)
{
    if(n%2) // even
        return 4.0/pow(kn<Float>(n),3);
    else
        return 0.0;
}

} // end of namespace noSlip

int main(int argc, char* args[])
{
    std::cout << "\nStarting startFlowNoSlip." << std::endl;
    std::vector<std::string> para(args, args+argc);
    if(para.size() != 3)
    {
        std::cout << "No target file provided. Call as\n"
          << "\tstartFlowNoSlip" << " N /path/to/noSlipCoeffs.csv" << std::endl;
        return 0;
    }

    using Float = boost::multiprecision::cpp_dec_float_50;
    std::cout << std::setprecision(std::numeric_limits<Float>::digits10);

    const auto N = std::size_t(std::stoi(para[1]));
    const std::string cppFilePath = args[2];

    std::cout
            << "\nSetup: " << '\n'
            << "N: " << N << "  "
            << std::endl;

    /*
     * writing the first N coefficients
     */
    std::ofstream file;
    file.open(cppFilePath, std::fstream::out);
    file << std::setprecision(std::numeric_limits<Float>::digits10);

    for(std::size_t n = 1; n<N+1; n++)
    {
        // write to file
        file << noSlip::kn<Float>(n) << "," << noSlip::An<Float>(n);
        if(n != N) file << '\n';
    }

    file.close();

    std::cout << "\nEnding startFlowNoSlip." << std::endl;
    return 0;
}
