#include <cstdint>  // std::size_t
#include <vector>
#include <tuple>
#include <algorithm> // std::sort
#include <iterator> // std::back_inserter

#include <boost/math/constants/constants.hpp> // boost::math::constants::pi
#include <boost/math/tools/roots.hpp> // boost::math::tools::bisect

namespace ssf {

template<typename Float>
const auto& StartupCoeffs<Float>::ans(const std::size_t N) {
    // caching for the an coefficients
    if(!anPtr_ || anPtr_->size() != N) 
    {
        calcAn(N);
    }

    return *anPtr_;
}

template<typename Float>
const auto& StartupCoeffs<Float>::kns(const std::size_t N) {
    if(!knPtr_ || knPtr_->size() != N)
        calcKn(N);
    return *knPtr_;
}

template<typename Float>
Float StartupCoeffs<Float>::charFunc(const Float kn) const 
{
    // Sp, Sm version of the characteristic equation for cos(2*kn) != 0.0
    return tan(2.0*kn) - (Sp() + Sm()) * kn / ( pow(kn,2)*Sp()*Sm() - 1.0 );
}

// 2**n can be represented exactly for 2**n within the range of Float. Hence, 
// the conversion to Float is not expected to raise problems.
template<typename Float>
void StartupCoeffs<Float>::calcKn(const std::size_t N) {
    using boost::math::constants::pi;

    // create/reinitialize vector to store the kns
    knPtr_.reset(new std::vector<Float>);
    knPtr_->reserve(N);

    // create phi which contains the initial set of interval boundaries
    std::vector<Float> phi;
    phi.reserve(N+1);
    auto bounds = [n=0] () mutable {return pi<Float>()*(2.0*n++ + 1.0) / 4.0;};
    std::generate_n(std::back_inserter(phi), N+1, bounds);

    // if either Sp or Sm are zero, then the characteristic equation reduces
    // to the case where where this spurious root does not occur
    if( Sp_ != Float(0.0) && Sm_ != Float(0.0) ) {
        auto phi_s = 1.0 / sqrt(Sp_ * Sm_);
        // if phi_s is larger than the largest phi_k in phi we can ignore it
        if(phi_s < *(phi.end()-1)) {     
            // location where to insert phi_s
            // NOTE: instead of computing n_s directly one could also iterate
            // through phi. This may be required when the computation
            // of n_s yields incorrect results due to rounding errors.
            // NOTE: floor is type-deduced automatically.
            auto approx_n_s = (4.0 / (pi<Float>() * sqrt(Sp()*Sm())) -1.0)/2.0;
            std::size_t n_s = floor(approx_n_s);

            phi.insert(phi.begin() + n_s + 1, phi_s);
            phi.pop_back(); // reduces the size of the container
        }
    }

    // find roots inbetween sorted singularities
    for(auto left = phi.begin(); left < phi.end()-1; ++left)
    {
        // we are using boost anyway so this does not hurt
        const Float min = boost::math::float_next(*left);
        const Float max = boost::math::float_prior(*(left+1));

        // The expressions on the right hand side should not be placed into
        // bisect directly, as the Float operations seem to tbe based on
        // something analog to expression templates - same problem for auto.
        // Explicit casting maybe be possible though.
        //  const Float min = *left + getEps();
        //  const Float max = *(left + 1) - getEps();
        using boost::math::tools::bisect;
        auto targetFunc = [=](Float p) {return this->charFunc(p);};
        const auto root = bisect(targetFunc, min, max, getTol());
        knPtr_->push_back( (root.first + root.second)/2.0 );
    }
}

template<typename Float>
Float StartupCoeffs<Float>::an (
    const Float Sp,
    const Float Sm,
    const Float kn
) const {
    const auto factor1 = 8.0 * sin(kn) * (sin(kn) + Sm * kn * cos(kn));
    const auto factor2 = pow(kn,2) * pow(Sp,2) + 1.0;
    const auto numer = factor1 * factor2;

    const auto term1 = 2.0 * pow(Sp,2) * pow(Sm,2) * pow(kn,4);
    const auto term2 = pow(Sp,2) * (Sm + 2.0) + pow(Sm,2) * (Sp + 2.0);
    const auto term3 = Sp + Sm + 2.0;
    const auto denom = pow(kn,3) * (term1 + term2 * pow(kn,2) + term3);
    return numer/denom;
}

template<typename Float>
void StartupCoeffs<Float>::calcAn(const std::size_t N) {

    // make sure the correct number of kns is available
    if(knPtr_->size() != N) {
        reset(); 
        calcKn(N);
    }
    // create/reinitialize space for an coefficients
    anPtr_.reset(new std::vector<Float>(knPtr_->size(), Float("0")));

    for(auto [itKn, itAn] = std::pair(knPtr_->begin(), anPtr_->begin());
        itKn != knPtr_->end(), itAn != anPtr_->end(); ++itKn, ++itAn) {
        *itAn = an(Sp(), Sm(), *itKn);
    }
}

} // end namespace ssf
