#ifndef STARTUPCOEFFS_HPP
#define STARTUPCOEFFS_HPP

#include <vector>
#include <memory>
#include <cstdint>

namespace ssf { // ssf = scaled starting flows

/*
*
* Computation of roots of the characteristic equation for the channel start-
* up solution with non-equal slip coefficients on both channel walls. By 
* changing the Float template parameter, arbitrary precision arithmetics, e.g.,
* provided by the boost library can be used.
*
* Applied patterns:
*   - lazy evaluation for the storage of the coefficients
*   - nested function object for the tolerance of the bisection method
*
*/
template<typename Float>
class StartupCoeffs
{
    public:
        StartupCoeffs() = delete;
        StartupCoeffs(const StartupCoeffs&) = delete;
        
        // S : slip length on both walls at y = +1,-1
        // bsTol : tolerance passed to the bisection method
        StartupCoeffs (const Float S, const Float bsTol) 
            : StartupCoeffs(S, S, bsTol){}

        // Sp : slip length on the wall at y = +1
        // Sm : slip length on the wall at y = -1
        // bsTol : tolerance passed to the bisection method
        StartupCoeffs(const Float Sp, const Float Sm, const Float bsTol)
            : Sp_(Sp), Sm_(Sm), bsTol_(bsTol) {}

        Float charFunc(const Float kn) const ;

        Float an (const Float Sp, const Float Sm, const Float kn) const;

        const auto& kns(const std::size_t N);
        const auto& ans(const std::size_t N);

        std::size_t size() const {return knPtr_->size();}
        void reset() { knPtr_.reset(); anPtr_.reset(); }

        void setSp(const Float Sp) { reset(); Sp_ = Sp; } 
        void setSm(const Float Sm) { reset(); Sm_ = Sm; } 

        const auto& Sp() const {return Sp_;}
        const auto& Sm() const {return Sm_;}
        auto getTol() const { return Tol<Float>(bsTol_); }

    private:

        // TODO replace with lambda and capture for tol
        // provides parameter dependent tolerance criterion as functor
        template<typename F>
        class Tol{
                F tol_;
            public:
                Tol(F tol) : tol_(tol) {}
                bool operator() (const F l, const F r){return abs(r-l) < tol_;}
        };

        Float Sp_;          // slip length to length scale ratio (radius)
        Float Sm_;          // slip length to length scale ratio (radius)
        Float eps_;         // epsilon for the offset to open intervals
        Float bsTol_;       // tolerance for the bisection method

        std::unique_ptr<std::vector<Float>> knPtr_;
        std::unique_ptr<std::vector<Float>> anPtr_;

        void calcKn(const std::size_t N);
        void calcAn(const std::size_t N);
};

} // ending namespace ssf

#include "startingFlows.cpp"

#endif
