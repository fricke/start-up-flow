// this is channelFlows.cpp
#include "startingFlows.hpp"

namespace cf {

//* y - scaled coordinate in the channel (cross section)
//* Sp - ratio of slip length to radius at y=+1
//* Sm - ratio of slip length to radius at y=-1
template<typename Float>
Float statPoiseuille
(
    const Float y,
    const Float Sp,
    const Float Sm
) {
    auto term1 = (3.0 * (Sp + Sm) + 4.0 * Sp * Sm + 2.0) / (Sp + Sm + 2.0);
    auto factor = 2.0 * (Sm - Sp) / (Sp + Sm + 2.0);
    return term1 - factor * y - pow(y, 2);
}

//* y - scaled coordinate in the channel (cross section)
//* S - ratio of slip length to radius at y=+1,-1
template<typename Float>
Float statPoiseuille (const Float y, const Float S) {
    return statPoiseuille(y, S, S);
}

//* t - scaled time
//* y - scaled coordinate in the channel (cross section)
//* S - ratio of slip length to radius at y=+1,-1
//* N - number of terms for the series solution
template<typename Float>
Float navierSlip
(
    const Float t,
    const Float y,
    const Float S,
    const std::size_t N = 10,
    const Float tol = 1e-16 // bisection tolerance
) {
    return navierSlip(t, y, S, S, N, tol);
}

//* t - scaled time
//* y - scaled coordinate in the channel (cross section)
//* Sp - ratio of slip length to radius at y=+1
//* Sm - ratio of slip length to radius at y=-1
//* N - number of terms for the series solution
//* TODO make a class from this for storage of the startup coefficients to speed
//* up a repeated use of the function at different t and x.
//*
//*/
template<typename Float>
Float navierSlip
(
    const Float t,
    const Float y,
    const Float Sp,
    const Float Sm,
    const std::size_t N = 10,
    const Float tol = 1e-16 // bisection tolerance
) {
    auto coeffs = ssf::StartupCoeffs<Float>(Sp, Sm, tol);
    auto kns = coeffs.kns(N);
    auto ans = coeffs.ans(N);

    Float series, sinTerm, cosTerm, expTerm = 0;
    for(auto [itKn, itAn] = std::tuple(kns.begin(), ans.begin());
        itKn != kns.end(),  itAn != ans.end(); ++itKn, ++itAn)
    {
        auto kn = *itKn;
        auto an = *itAn;

        sinTerm = sin( kn * (y + 1.0) );
        cosTerm = Sm * kn * cos(kn * (y + 1.0) );
        expTerm = exp(-pow(kn,2) * t);
        series += an * (sinTerm + cosTerm) * expTerm;
    }
    return statPoiseuille(y, Sp, Sm) - series;
}


/* Analytic start-up Poiseuille-flow velocity between two plates with
* Navier slip boundary condition on both walls. Zero velocity at t=0.
*
//* t - scaled time
//* x - scaled coordinate in the channel (cross section)
//* S - ratio of slip length to radius (half diameter of the channel)
//* N - number of terms for the series solution
//* TODO make a class from this for storage of the startup coefficients to speed
//* up a repeated use of the function at different t and x.
//*
//*/
//template<typename Float>
//Float navierSlip
//(
//    const Float t,
//    const Float x,
//    const Float S,
//    const std::size_t N=10,
//    const Float tol = 1e-16,
//    const Float eps = 1e-16
//)
//{
//    auto coeffs = ssf::StartupCoeffs<Float>(S, tol, eps);
//    auto Kn = coeffs.Kn(N); // must be computed before An
//    auto An = coeffs.An(Kn);
//
//    Float series, sinTerm, cosTerm, expTerm = 0;
//    for(auto [itKn, itAn] = std::tuple(Kn.begin(), An.begin());
//        itKn != Kn.end(),  itAn != An.end(); ++itKn, ++itAn)
//    {
//        auto kn = *itKn;
//        auto an = *itAn;
//
//        sinTerm = sin( kn * (x + 1.0) );
//        cosTerm = S*kn * cos(kn * (x + 1.0) );
//        expTerm = exp(-pow(kn,2) * t);
//        series += an * (sinTerm + cosTerm)*expTerm;
//    }
//    return 1.0 - pow(x,2) / (2.0 * S + 1.0) - series / (2.0 * S + 1.0);
//}

} // namespace cf
