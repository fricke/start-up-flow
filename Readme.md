# Arbitrary Accuracy Computation for Starting Flow Solution Coefficients
This repository contains an implementation of the solution algorithm for the
starting flow in a channel with arbitrary, non equal slip length on the
channel walls. Test cases are based on OpenFOAM and evaluation tools on python
and jupyter notebooks.

The model for starting flow can be solved by a series solution that requires the
solution of a non linear characteristic equation. The algorithms provided in this
repository automatically solves the non-linear characteristic equation and 
computes the full velocity field with verified maximum accuracy.

## How to cite
Further information on the model, the algorithm, the verification study, and a
comparison to literature values can be found
[here](https://arxiv.org/abs/2103.06662v2). If the implementation or our paper 
have been useful to you, we would kindly ask to cite our work via

    @article{Raju2022,
      title={Computing hydrodynamic eigenmodes of channel flow with slip -- A highly accurate algorithm}, 
      author={ Raju, S. and Gründing, D. and Marić, T. and Bothe, D. and Fricke, M.},
      year={2022},
      journal={The Canadian Journal of Chemical Engineering},
      eprint={2103.06662v2},
      archivePrefix={arXiv},
      doi = {10.48550/ARXIV.2103.06662},
      url = {https://arxiv.org/abs/2103.06662},
      primaryClass={physics.flu-dyn}
    }

## Model
The algorithm provides the analytic solution for the starting flow for 
$$\partial_t u = \nu \partial_{xx} u + G$$
where G is the pressure gradient driving the flow. As initial conditions
u=0 is used while at the top and bottom of the channel are
$$\mp L^\pm \partial_y u &= u \quad &&\text{at} \quad y=\pm R ~\text{for}~ t \geq 0$$
is applied.

Two implemenations with different accuracy are available:

1. A python implementation that provides a solution accuracy up to $$10^-16$$
for the series coefficients
2. A C++-implementation that can provide arbitrary accuracy based as it is
implemented using the boost arbitrary precision library. For this purpose a 
template based implementation is provided.

Both implementations can be used as black box implementations. The solution 
algorithm provides a maximum accuracy while being orders of magnitudes faster 
than a full CFD-solution.

## Installation and Dependencies
On ubuntu 20.04 the program should run out of the box and can be installed by
changing into the main folder start-up-solution and executing

    ./install.sh

See below for additional dependencies.

## Python
### Conda package manager
The current setup uses an installation that is based on the conda package manager.
See [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)
for instructions on how to install the conda package manager.
If not already install, jupyter notebooks and the scipy package can be installed via the conda package manager. Open a terminal and execute

    conda install -c anaconda scipy 

Depending on your setup, you may need to execute first

    conda activate

To use jupyter notebooks execute as `jupyter-notebook notebook_name.ipynb`

### Ubuntu Package Manager
Alternatively, the required packages can be installed in ubuntu using the package manager

    sudo get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose

See the [scipy website](https://www.scipy.org/install.html) for furhter information.

## C++
The C++ implementation is based on the boost arbitrary precision arithmetics library.
Hence, the implementation is based on the template parameter `Float` and can therefor
also be run with standard floating point numbers such as `double`. For dependencies
and installation instructions see below.

The project depends on the following libraries and utilities. See below for
linux installation instructions.

- boost (providing arbitrary precision arithmetics)
- cmake (used to compile the project)

### Boost
Boost can be installed from the packages

    sudo apt install libboost-all-dev

A description of relevant boost-functions used to obtain the next and prior floating points numbers can be found at:

- [float_prior](https://www.boost.org/doc/libs/1_61_0/libs/math/doc/html/math_toolkit/next_float/float_prior.html)
- [float_next](https://www.boost.org/doc/libs/1_61_0/libs/math/doc/html/math_toolkit/next_float/float_next.html)

With more examples [here](https://www.boost.org/doc/libs/1_59_0/libs/multiprecision/doc/html/boost_multiprecision/tut/limits/functions.html).

### cmake
Cmake can be installed from the package list

    sudo apt-get install build-essential
    sudo apt install cmake

After installing above dependencies/utilities, source and build in the main folder :

    . etc/bashrc
    ./install.sh

## Test cases
All test cases have been evaluated using python inside jupyter notebooks. As
verification basis the OpenFOAM solver icoFoam has been used to obtain simulation
results of the full Navier Stokes equation in the channel. 
All evaluation tools have been implemented in python within jupyter notebooks. 
These evaluation tools can be found in the folder `python/notebooks`.

To compute coefficients and run the test utility execute below command. Parameters for obtaining the coefficients can be set in the same run.sh file.  

    ./run.sh
